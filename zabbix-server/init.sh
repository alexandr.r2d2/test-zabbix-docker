#!/bin/bash

#Install mysql
apt-get install -y mysql-server

#Starting service MySQL
service mysql start

#Starting services Zabbix+Apache
service apache2 start
service zabbix-server start
service zabbix-agent start

#just keep working!
ping 127.0.0.1
