Настроен билд и перенос образа в dockerhub. Конфигурационный файл .gitlab-ci.yml

Для запуска билда и запуска контейнера локально необходимо выполнить run_me.sh

Конфигурация запуска задана в файле docker-compose.yml

В Dockerfile описан процес установки Zabbix Server, копирование конфигурацинных файлов и процедура запуска.

При запуске выполняется файл init.sh, который устанавливает MySQL Server и запускает необходимые для работы службы.

В каталоге  config находятся файлы конфигурации Zabbix и Apache.

Каталог mysql подключается к контейнеру как /var/lib/mysql